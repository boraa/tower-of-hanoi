import { Hanoi } from './Hanoi';
import { GameObject } from './Common/GameObject';
import { Game } from './Common/Game';
import { StartFrame } from './StartFrame';
import { GameState } from './GameState';
import { EndFrame } from './EndFrame';

window.onload = function() {
    const canvas = <HTMLCanvasElement>document.getElementById('viewport');

    const gameObjects: GameObject[] = [
        new Hanoi(7),
        new StartFrame(),
        new EndFrame()
    ];
    const game: Game = new Game(canvas, 'Tower of Hanoi', gameObjects, GameState.NotStarted);
    
    game.start();
};