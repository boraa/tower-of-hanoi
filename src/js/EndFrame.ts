import { GameObject } from './Common/GameObject';
import { Renderable } from './Common/Renderable';
import { GameState } from './GameState';

export class EndFrame extends GameObject implements Renderable {
    
    init(canvas: HTMLCanvasElement): void {
        canvas.addEventListener('click', x => {
            if (this.gameState === GameState.Finished) {
                this.gameState = GameState.Restart;
            }
         });
    }

    render(context: CanvasRenderingContext2D): void {
        if (this.gameState === GameState.Finished) {
            context.fillStyle = 'rgba(0, 0, 0, 0.8)';
            context.fillRect(0, 0, this.gameWidth, this.gameHeight);
            
            context.fillStyle = '#fff';
            context.font = '24px Verdana';
            this.drawScreenCenterText(context, 'Finished. Click to Start Again!');
        }
    }
}