export interface Move {
    from: number;
    to: number;
}