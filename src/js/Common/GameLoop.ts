import { GameEvents } from './GameEvents';

export class GameLoop {
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    gameEvents: GameEvents;

    width: number;
    height: number;
    gameState: number;

    // Timing and frames per second
    lastframe: number = 0;
    fpstime: number = 0;
    framecount: number = 0;
    fps: number = 0;

    initialized = false;

    constructor(canvas: HTMLCanvasElement, gameEvents: GameEvents, gameState: number = 0) {
        this.canvas = canvas;
        this.width = canvas.width;
        this.height = canvas.height;
        
        this.context = canvas.getContext('2d');

        this.gameEvents = gameEvents;
        this.gameState = gameState;
    }

    public start() {
        this.gameEvents.init();

        // Enter main loop
        this.main(0);
    }

    private main(timestamp: number) {
        // Request animation frames
        window.requestAnimationFrame(t => this.main(t));

        if (!this.initialized) {
            const canvas = this.canvas;

            // Clear the canvas
            this.context.clearRect(0, 0, canvas.width, canvas.height);
            
            this.initialized = true;
        } 

        // Update and render the game
        this.update(timestamp);
        this.render();
    }

    // Update the game state
    private update(timestamp: number) {
        let dt = (timestamp - this.lastframe) / 1000;
        this.lastframe = timestamp;
        
        // Update the fps counter
        this.updateFps(dt);

        this.gameEvents.update(dt);
    }

    // Render the game
    private render() {
        this.gameEvents.render(this.context);
    }

    private updateFps(dt: number) {
        if (this.fpstime > 0.25) {
            // Calculate fps
            this.fps = Math.round(this.framecount / this.fpstime);
            
            // Reset time and framecount
            this.fpstime = 0;
            this.framecount = 0;
        }
        
        // Increase time and framecount
        this.fpstime += dt;
        this.framecount++;
    }
}