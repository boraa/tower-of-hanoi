import { GameObject } from './GameObject';
import { GameLoop } from './GameLoop';
import { GameFrame } from './GameFrame';
import { Updateable } from './Updateable';
import { Renderable } from './Renderable';

export class Game {

    gameLoop: GameLoop;
    gameObjects: GameObject[] = [];

    constructor(canvas: HTMLCanvasElement, name: string, gameObjects: GameObject[], gameState: number = 0) {
        this.gameLoop = new GameLoop(canvas, { init: () => {}, update:  t => this.update(<number>t), render: c => this.render(c) }, gameState);
        
        const gameFrame = new GameFrame(name);
        gameFrame.setGameLoop(this.gameLoop);
        gameFrame.init(canvas);
        this.gameObjects.push(gameFrame);

        for (let i = 0; i < gameObjects.length; i++) {
            const gameObject = gameObjects[i];
            gameObject.setGameLoop(this.gameLoop);
            gameObject.init(canvas);

            this.gameObjects.push(gameObject);
        }
    }

    start() {
        this.gameLoop.start();
    }

    update(dt: number): void {
        for (let i = 0; i < this.gameObjects.length; i++) {
            const gameObject = this.gameObjects[i] as unknown as Updateable;
            if (gameObject.update) {
                gameObject.update(dt);
            }
        }
    }

    render(context: CanvasRenderingContext2D): void {
        for (let i = 0; i < this.gameObjects.length; i++) {
            const gameObject = this.gameObjects[i] as unknown as Renderable;
            if (gameObject.render) {
                gameObject.render(context);
            }
        }
    }
}