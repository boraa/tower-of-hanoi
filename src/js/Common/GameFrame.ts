import { Renderable } from './Renderable';
import { GameObject } from './GameObject';

export class GameFrame extends GameObject implements Renderable {

    gameName: string;

    constructor(gameName: string) {
        super();
        
        this.gameName = gameName;
    }

    render(context: CanvasRenderingContext2D): void {
        // Draw background
        context.fillStyle = '#e8eaec';
        context.fillRect(0, 0, this.gameWidth, this.gameHeight);
        
        // Draw header
        context.fillStyle = '#303030';
        context.fillRect(0, 0, this.gameWidth, 79);
        
        // Draw title
        context.fillStyle = '#ffffff';
        context.font = '24px Verdana';
        context.fillText(this.gameName, 10, 37);
        
        // Display fps
        context.fillStyle = '#ffffff';
        context.font = '12px Verdana';
        context.fillText('Fps: ' + this.fps, 13, 57);

        // Display moves count
        context.fillStyle = '#ffffff';
        context.font = '18px Verdana';

        // Draw ground
        context.fillStyle = '#ccc';
        context.fillRect(0, this.gameHeight - 5, this.gameHeight, 5);
    }
}