export interface Updateable {
    update(dt: number): void;
}