import { GameLoop } from './GameLoop';
import { TextSize } from './TextSize';

export class GameObject {

    gameLoop: GameLoop;
    
    public get gameWidth(): number {
        return this.gameLoop.width;
    }
    
    public get gameHeight(): number {
        return this.gameLoop.height;
    }

    public get gameState(): number {
        return this.gameLoop.gameState;
    }

    public set gameState(gameState: number) {
        this.gameLoop.gameState = gameState;
    }

    public get fps(): number {
        return this.gameLoop.fps;
    }

    setGameLoop(gameLoop: GameLoop) {
        this.gameLoop = gameLoop;
    }

    init(canvas: HTMLCanvasElement) {
    }

    drawCenterText(context: CanvasRenderingContext2D, text: string, x: number, y: number, width: number): void {
        const textSize = this.measureText(context, text);
        context.fillText(text, x + (width - textSize.width) / 2, y);
    }

    drawScreenCenterText(context: CanvasRenderingContext2D, text: string): void {
        const textSize = this.measureText(context, text);
        context.fillText(text, (this.gameWidth - textSize.width) / 2, (this.gameHeight + textSize.height) / 2);
    }

    measureText(context: CanvasRenderingContext2D, text: string): TextSize {
        const textSize = context.measureText(text);
        const textHeight = textSize.actualBoundingBoxAscent + textSize.actualBoundingBoxDescent;
        return {
            width: textSize.width,
            height: textHeight
        };
    }
}