export interface GameEvents {
    init: () => void;

    update: (dt: number) => void;

    render: (context: CanvasRenderingContext2D) => void;
}