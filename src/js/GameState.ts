export class GameState {
    static NotStarted: number = 0;
    static Started: number = 1;
    static Finished: number = 2;
    static Restart: number = 3;
}