import { Renderable } from './Common/Renderable';

export class Disk implements Renderable {
    color: string;
    width: number;
    height: number;
    x: number;
    y: number;

    constructor(size: number, color: string) {
        this.color = color;
        this.width = size * 12;
        this.height = 30;
        this.x = 0;
        this.y = 0;
    }
    
    setPosition(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }

    render(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}