import { GameObject } from './Common/GameObject';
import { Renderable } from './Common/Renderable';
import { GameState } from './GameState';

export class StartFrame extends GameObject implements Renderable {
    
    init(canvas: HTMLCanvasElement): void {
        canvas.addEventListener('click', e => {
            const rect = canvas.getBoundingClientRect();
            const x = e.clientX - rect.left;
            const y = e.clientY - rect.top;
            
            if (this.gameState === GameState.NotStarted) {
                this.gameState = GameState.Started;
            }
         });
    }

    render(context: CanvasRenderingContext2D): void {
        if (this.gameState === GameState.NotStarted) {
            context.fillStyle = 'rgba(0, 0, 0, 0.8)';
            context.fillRect(0, 0, this.gameWidth, this.gameHeight);
            
            context.fillStyle = '#fff';
            context.font = '24px Verdana';
            this.drawScreenCenterText(context, 'Click to Start!');
        }
    }
}