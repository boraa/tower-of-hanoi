import { Rod } from './Rod';
import { Move } from './Move';
import { Disk } from './Disk';
import { Renderable } from './Common/Renderable';
import { Updateable } from './Common/Updateable';
import { GameObject } from './Common/GameObject';
import { GameState } from './GameState';

export class Hanoi extends GameObject implements Renderable, Updateable {

    rods: Rod[] = [];
    colors = ['#FF0000', '#FF7F00', '#FFD400', '#FFFF00', '#BFFF00', '#6AFF00', '#00EAFF', '#0095FF', '#0040FF', '#AA00FF', '#FF00AA', '#EDB9B9', 
              '#E7E9B9', '#B9EDE0', '#B9D7ED', '#DCB9ED', '#8F2323', '#8F6A23', '#4F8F23', '#23628F', '#6B238F', '#000000', '#737373', '#CCCCCC'];

    diskCount: number;
    moves: Move[] = [];

    movesCount: number = 0;
    waitTime: number = 0;
    animationSpeedInMs: number = 1000;

    constructor(diskCount: number) {
        super();

        this.diskCount = diskCount;
    }

    init() {
        for (let i = 0; i < 3; i++) {
            const rod = new Rod(i, this.gameHeight);
            if (i === 0) {
                for (let j = 0; j < this.diskCount; j++) {
                    rod.addDisk(new Disk(12 - j, this.colors[j]));
                }
            }

            this.rods.push(rod);
        }

        this.calculateMoves(this.diskCount, 0, 1, 2);
        this.moves = this.moves.reverse();
    }

    update(dt: number): void {
        if (this.gameState === GameState.Restart) {
            this.reset();
            this.gameState = GameState.Started;
        }

        if (this.gameState !== GameState.Started) {
            return;
        }

        if (this.hasMoves()) {
            if (this.waitTime === null) {
                this.waitTime = dt * 1000;
            }
            else {
                this.waitTime += dt * 1000;
                if (this.waitTime >= this.animationSpeedInMs) {
                    this.waitTime = 0;

                    this.updateHanoi();

                    this.movesCount++;
                }
            }
        } else {
            this.gameState = GameState.Finished;
        }
    }

    render(context: CanvasRenderingContext2D): void {
        if (this.gameState === GameState.NotStarted) {
            return;
        }

        for (let i = 0; i < this.rods.length; i++) {
            this.rods[i].render(context);
        }

        const movesText = 'Moves:';
        const movesTextSize = context.measureText(movesText);

        context.fillStyle = '#ffffff';
        context.fillText(movesText, 400, 32);

        context.fillStyle = '#ffffff';
        context.font = '24px Verdana';
        this.drawCenterText(context, this.movesCount.toString(10), 400, 60, movesTextSize.width);
    }

    calculateMoves(n: number, x: number, y: number, z: number): void {
        if (n === 0) {          
            return;
        }

        this.calculateMoves(n - 1, x, z, y);
        this.moves.push({ from: x, to: z });
        this.calculateMoves(n - 1, y, x, z);
    }

    hasMoves(): boolean {
        return this.moves.length > 0;
    }

    updateHanoi(): void {
        let move = this.moves.pop();
        if (move) {
            let disk = this.rods[move.from].popDisk();
            this.rods[move.to].addDisk(disk);
        }
    }

    reset(): void {
        this.rods = [];
        this.movesCount = 0;

        this.init();
    }
}