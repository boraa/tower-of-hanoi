import { Disk } from './Disk';
import { Renderable } from './Common/Renderable';

export class Rod implements Renderable {
    width: number;
    height: number;
    number: number;
    color: string;
    x: number;
    y: number;
    disks: Disk[];

    constructor(number: number, canvasHeight: number) {
        this.width = 4;
        this.height = 370;
        this.number = number;
        this.color = '#000';
        this.x = 10 + (160 - this.width) / 2 + 160 * number;
        this.y = canvasHeight - this.height;
        this.disks = [];
    }

    addDisk(disk: Disk): void {
        const diskNumber = this.disks.length + 1;
        disk.setPosition(this.x - (disk.width / 2) + (this.width / 2), (this.y + this.height) - (diskNumber * disk.height));
        this.disks.push(disk);
    }

    popDisk(): Disk {
        return this.disks.pop();
    }

    render(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height - 5);
        for (let i = 0; i < this.disks.length; i++) {
            this.disks[i].render(ctx);
        }
    }
}