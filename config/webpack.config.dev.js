const helpers = require('./helpers'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      webpackConfig = require('./webpack.config.base');

webpackConfig.plugins = [...webpackConfig.plugins,
  new HtmlWebpackPlugin({
    inject: true,
    template: helpers.root('/src/index.html')
  })
];

webpackConfig.devServer = {
  port: 8080,
  host: 'localhost',
  historyApiFallback: true,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
    ignore: /node_modules/
  },
  contentBase: './src',
  open: true
};

webpackConfig.mode = 'development';

module.exports = webpackConfig;
