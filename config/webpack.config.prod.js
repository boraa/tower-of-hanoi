const helpers = require('./helpers'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
      BomPlugin = require('webpack-utf8-bom'),
      webpackConfig = require('./webpack.config.base');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

webpackConfig.optimization = {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        extractComments: true,
        uglifyOptions: {
            keep_fnames: true,
            output: {
              comments: false,
              beautify: false
            },
            compress: {
                drop_console: true
            }
          }
      })
    ],
    splitChunks: {
        cacheGroups: {
            commons: {
                test: /[\\/]node_modules[\\/]/,
                name: "vendors",
                chunks: "all"
            }
        }
    }
};

webpackConfig.plugins = [...webpackConfig.plugins,
new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: [helpers.root('./dist')] }),
new HtmlWebpackPlugin({
    inject: true,
    template: helpers.root('/src/index.html'),
    minify: true
  }),
new BomPlugin(true)
];

webpackConfig.mode = 'production';

module.exports = webpackConfig;
