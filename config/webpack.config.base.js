const helpers = require('./helpers');

let config = {
  entry: {
    'main': helpers.root('/src/js/Main.ts')
  },
  output: {
    path: helpers.root('./dist'),
    filename: 'js/[name].[hash].js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js', '.html']
  },
  resolveLoader: {
    modules: [
      'node_modules'
    ]
  },
  module: {
    rules: [{
      test: /\.ts$/,
      exclude: /node_modules/,
      enforce: 'pre',
      loader: 'tslint-loader'
    },
    {
      test: /\.ts$/,
      loader: 'ts-loader'
    },
    {
      test: /\.html$/,
      loader: 'raw-loader'
    }
    ],
  },
  plugins: []
};

module.exports = config;
